package config

import (
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// RouteDb has the instance of Router and DB
type RouteDb struct {
	Router *mux.Router
	DB     *gorm.DB
}
