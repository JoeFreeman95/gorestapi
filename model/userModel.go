package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type User struct {
	gorm.Model
	id        int    `gorm: "primary_key json:"id"`
	firstName string `json: "firstName"`
	lastName  string `json: "lastName"`
	email     string `json: "email"`
	Address   Address
}

type Users []User

var user User

func DBUserMigration(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&User{})
	db.Model(&user).Related(&address)
	return db
}
