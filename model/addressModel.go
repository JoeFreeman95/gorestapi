package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Address struct {
	gorm.Model
	id          int    `gorm: "primary_key json:"id"`
	houseNumber string `json: "houseNumber"`
	firstLine   string `json: "firstLine"`
	secondLine  string `json: "secondLine"`
	postCode    string `json: "postCode"`
}

var address Address

func DBAddressMigration(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(&Address{})
	return db
}
