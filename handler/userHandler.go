package handler
 
import (
	"encoding/json"
	"net/http"
 
	"github.com/GoRestApi/model"
	"github.com/GoRestApi/common"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

func GetAllUsers(db *gorm.DB, w http.ResponseWriter, r *http.Request {
	users := []model.User{}
	db.Find(&users)
	common.jsonResponse(w, http.StatusOK, users)
}

func createUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	user := model.User{}
	
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		   common.errorResponse(w, http.StatusBadRequest, err.Error()
		   return
	}
	defer r.Body.Close()
	
	if err := db.Save(&user).Error; err != nil {
		common.errorResponse(w,http.StatusInternalServerError, err.Error())
		return
	}
	common.jsonResponse(w,http.StatusOK, user)
}